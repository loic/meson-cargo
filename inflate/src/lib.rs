extern "C" {
    fn vlc_foo(a: i32, b: i32) -> i32;
}

#[no_mangle]
pub extern "C" fn inflate_foo(left: i32, right: i32) -> i32 {
    // left + right
    unsafe { vlc_foo(left, right) }
}
