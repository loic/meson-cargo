#!/bin/bash

set -e

# CARGO_RUSTC_CMD=${@:1:$#-2} # all other args
CARGO_RUSTC_CMD=${@:1:$#-1} # all other args
# MODULE_PROJECT_DIR=${@:$#-1:1} # second last argument
OUTPUT=${@:$#:1} # last argument
# PREFIX_OUTPUT_FILES=${@:$#:1} # last argument

# Create the target directory
mkdir -p "$CARGO_TARGET_DIR"

# Build the crate
$CARGO_RUSTC_CMD

# Copy the files
cp "$CARGO_TARGET_DIR/$CARGO_BUILD_TARGET/release/$OUTPUT.a" "$OUTPUT.a"
# cp "$CARGO_TARGET_DIR/$CARGO_BUILD_TARGET/release/$OUTPUT.so" "$OUTPUT.so"
cp "$CARGO_TARGET_DIR/$CARGO_BUILD_TARGET/release/$OUTPUT.d" "$OUTPUT.d"
