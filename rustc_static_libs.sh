#!/bin/bash

set -e

TMP_DIR=$(mktemp)
echo "#![allow(dead_code)] fn main(){}" | rustc --target x86_64-unknown-linux-gnu --crate-type staticlib --print native-static-libs -o "$TMP_DIR" - 2>&1 | grep "native-static-libs" | sed "s/note: native-static-libs://"
